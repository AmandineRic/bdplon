<?php

namespace App\Controller;

use App\Entity\BD;
use App\Repository\BDRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BDController extends AbstractController
{
    /**
     * @Route("/", name="b_d")
     */
    public function index(BDRepository $repo)
    {
        return $this->render('bd/index.html.twig', [
            'bd' => $repo->findAll()
        ]);
    }
    /**
     * @Route("/add-bd", name="add_bd")
     */
    public function addBd(Request $request, ObjectManager $manager) {
        $bd = new BD();

        $form = $this->createForm(BdType::class, $bd);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /* On peut accéder au user actuellement connecté depuis
            les controllers en utilisant la méthode getUser()
            qui nous renvoie une instance de l'entité User, on peut
            donc accéder à tous les getters/setter de ce user
            */
            $bd->setAuthor($this->getAuthor());
            $manager->persist($bd);
            $manager->flush();

        }

        return $this->render('bd/add.html.twig', [
            'form' => $form->createView()
        ]);

    }
}