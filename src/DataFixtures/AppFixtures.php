<?php

namespace App\DataFixtures;

use App\Entity\BD;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       for ($i=1; $i < 6; $i++) { 
           $bd = new BD();
           $bd->setTitle("Title ".$i)
              ->setAuthor("Author ".$i)
              ->setDescription("Descricption")
              ->setParition(\DateTime);
              $manager->persist($bd);
       }
        $manager->flush();
    }
}